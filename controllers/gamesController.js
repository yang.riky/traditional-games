const createError = require('http-errors');
const gamesRepository = require('../repositories/gamesRepository');

class GamesController {

    async getAllGames(req, res, next) {
        try {
            let [err, userGameHistories] = await gamesRepository.getAllGames();

            if (userGameHistories) {
              res.render('games/index', {
                  title: 'Games',
                  sectionTitle: 'List of Games',
                  button: '',
                  userGameHistories
              });
            } else {
              next(createError(400));
            }
        }
        catch (err) {
            next(createError(400));
        }
    }
}

const gamesController = new GamesController();

module.exports = gamesController;
