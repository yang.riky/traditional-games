const jwt = require('jsonwebtoken');

function checkJwtAuthentication(req, res, next) {
    try {
        if (!req?.headers?.authorization) {
            res.status(401).json({
                msg: 'Invalid token'
            });
        } else {
            const token = req?.headers?.authorization.split(" ")[1];
            const isTokenValid = jwt.verify(token, "secret_key");

            req.user = {
                id: isTokenValid.id,
            };
            next();
        }
    }
    catch (err) {
        res.status(401).json({
            err
        });
    }
}

module.exports = checkJwtAuthentication;