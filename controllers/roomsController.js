const roomsRepository = require('../repositories/roomsRepository');
const gamesRepository = require('../repositories/gamesRepository');

class RoomsController {

    // async generateRoomCode(length) {
    //     const CHARACTERS = "abcdefghijklmnopqrstuvwxyz0123456789";
    //     const CHARACTERS_LENGTH = CHARACTERS.length;
    //     let roomCode = "";
    //     let isRoomCodeExist = 0;

    //     do {
    //         roomCode = "";
    //         for (let i = 0; i < length; i++) {
    //             roomCode += CHARACTERS.charAt(Math.floor(Math.random() * CHARACTERS_LENGTH));
    //         }

    //         [err, isRoomCodeExist] = await roomsRepository.countRoomCode(roomCode);
    //     }
    //     while (isRoomCodeExist);

    //     return roomCode;
    // }

    async getAllRooms(req, res, next) {
        try {
            const [err, rooms] = await roomsRepository.getAllRoomsWithHistory(req.user.id);

            if (err) {
                res.status(400).json({
                    err
                });
            } else {
                res.status(200).json({
                    rooms
                });
            }
        } catch (error) {
            res.status(400).json({
                error
            })
        }
    }

    async createRoom(req, res, next) {
        try {
            let length = 5;

            const CHARACTERS = "abcdefghijklmnopqrstuvwxyz0123456789";
            const CHARACTERS_LENGTH = CHARACTERS.length;
            let roomCode = "";
            let isRoomCodeExist = 0;

            do {
                roomCode = "";
                for (let i = 0; i < length; i++) {
                    roomCode += CHARACTERS.charAt(Math.floor(Math.random() * CHARACTERS_LENGTH));
                }

                const [err, isRoomCodeExistRepo] = await roomsRepository.countRoomCode(roomCode);
                isRoomCodeExist = isRoomCodeExistRepo;
            }
            while (isRoomCodeExist);

            const [err, gameRoom] = await roomsRepository.findOrCreateRoomByUserGameId(
                req.body.roomName,
                roomCode,
                req.user.id
            );

            res.status(200).json({
                msg: "Room created",
                gameRoom
            });
        }
        catch (err) {
            res.status(401).json({
                err
            });
        }
    }

    async joinRoom(req, res, next) {
        try {
            const [err, isOwnRoom] = await roomsRepository.countRoomCodeByUserId(
                req.params.roomCode,
                req.user.id
            );

            if (isOwnRoom) {
                res.status(400).json({
                    msg: 'You cannot join your own game room'
                });
            } else {
                const [err, isRoomCodeExist] = await roomsRepository.getRoomByRoomCode(req.params.roomCode);

                if (isRoomCodeExist) {
                    const [err, updateRoomByOpponentId] = await roomsRepository.updateRoomByOpponentId(isRoomCodeExist.id, req.user.id);

                    res.status(200).json({
                        msg: 'Successfully joined room'
                    });
                } else {
                    res.status(404).json({
                        msg: 'Room not found'
                    });
                }
            }
        }
        catch (err) {
            res.status(401).json({
                err
            });
        }
    }

    async fightRoom(req, res, next) {
        try {
            const [err, isYourRoom] = await roomsRepository.getRoomForFight(
                req.params.roomCode,
                req.user.id
            );

            if (isYourRoom) {
                const [err, lastGameRound] = await gamesRepository.getLastGameRound(
                    isYourRoom.id,
                    req.user.id
                );

                if (err) {
                    res.status(400).json({
                        err
                    });
                } else {
                    let gameRound = 1;

                    if (lastGameRound) {
                        gameRound = lastGameRound.gameRound;
                    }

                    const [errIsGameRoundEnd, isGameRoundEnd] = await gamesRepository.countGameRound(
                        isYourRoom.id,
                        gameRound
                    );

                    if (errIsGameRoundEnd) {
                        res.status(400).json({
                            errIsGameRoundEnd
                        });
                    } else {
                        let play = true;
                        if (isGameRoundEnd === 2) {
                            gameRound++;

                            if (gameRound > 3) {
                                play = false;
                                const [err, result] = await roomsRepository.getRoomWithHistory(isYourRoom.id);
                                res.status(200).json({
                                    msg: "Game ended",
                                    result
                                });
                            }
                        } else {
                            const [errIsPlay, isPlay] = await gamesRepository.countGameRoundByUserGameId(
                                isYourRoom.id,
                                gameRound,
                                req.user.id
                            );

                            if (errIsPlay) {
                                res.status(400).json({
                                    errIsPlay
                                });
                            } else if (isPlay === 1) {
                                play = false;
                                res.status(200).json({
                                    msg: "Waiting opponent's choice for this round c"
                                });
                            }
                        }

                        if (play) {

                            const [errGame, game] = await gamesRepository.addGame(
                                req.user.id,
                                req.body.choice.toUpperCase(),
                                isYourRoom.id,
                                gameRound
                            );

                            if (game) {
                                const [errOpponentGame, opponentGame] = await gamesRepository.getOpponentGame(
                                    isYourRoom.id,
                                    gameRound,
                                    req.user.id
                                );

                                if (errOpponentGame) {
                                    res.status(400).json({
                                        errOpponentGame
                                    });
                                } else if (opponentGame) {
                                    let userChoice = game.userChoice
                                        .replace("ROCK", "0")
                                        .replace("PAPER", "1")
                                        .replace("SCISSORS", "2");
                                    let opponentChoice = opponentGame.userChoice
                                        .replace("ROCK", "0")
                                        .replace("PAPER", "1")
                                        .replace("SCISSORS", "2");

                                    let userResult = "";
                                    let opponentResult = "";
                                    let diff = userChoice - opponentChoice;

                                    switch (diff) {
                                        case -1:
                                        case 2:
                                            userResult = "WIN";
                                            opponentResult = "LOSE";
                                            break;

                                        case 1:
                                        case -2:
                                            userResult = "LOSE";
                                            opponentResult = "WIN";
                                            break;

                                        default:
                                            userResult = "DRAW";
                                            opponentResult = "DRAW";
                                            break;
                                    }

                                    let userScore = userResult
                                        .replace("DRAW", "0")
                                        .replace("LOSE", "0")
                                        .replace("WIN", "1");
                                    let opponentScore = opponentResult
                                        .replace("DRAW", "0")
                                        .replace("LOSE", "0")
                                        .replace("WIN", "1");

                                    await gamesRepository.updateGameResultAndScore(
                                        game.id,
                                        userResult,
                                        userScore
                                    );

                                    await gamesRepository.updateGameResultAndScore(
                                        opponentGame.id,
                                        opponentResult,
                                        opponentScore
                                    );

                                    const [err, result] = await roomsRepository.getRoomWithHistory(isYourRoom.id);

                                    res.status(200).json({
                                        msg: "Success",
                                        result
                                    });
                                } else {
                                    res.status(200).json({
                                        msg: "Waiting opponent's choice for this round b",
                                        game
                                    });
                                }
                            } else {
                                res.status(400).json({
                                    msg: "Failed"
                                });
                            }
                        }
                    }

                }
            } else {
                res.status(200).json({
                    msg: 'This is not your room'
                });
            }
        } catch (error) {
            console.log(error);
            res.status(400).json({
                error
            });
        }
    }
}

const roomsController = new RoomsController();

module.exports = roomsController;
