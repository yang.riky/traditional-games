'use strict';

const bcrypt = require('bcrypt');

async function hashPassword(password) {
  const SALT_ROUNDS = 10;

  return await bcrypt.hash(password, SALT_ROUNDS);
}

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserAdmin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  UserAdmin.init({
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notNull: true,
        notEmpty: true,
        len: [3, 255]
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notNull: true,
        notEmpty: true,
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
        len: [6, 20]
      }
    }
  }, {
    hooks: {
      beforeCreate: async (userGame, options) => {
        userGame.password = await hashPassword(userGame.password);
      },
      beforeUpdate: async (userGame, options) => {
        userGame.password = await hashPassword(userGame.password);
      }
    },
    sequelize,
    modelName: 'UserAdmin',
    tableName: 'user_admins',
    underscored: true,
    paranoid: true
  });
  return UserAdmin;
};