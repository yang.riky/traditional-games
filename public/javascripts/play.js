import { Player } from "./models/Player.js";
import { Computer } from "./models/Computer.js";
import { RPS } from "./models/RPS.js";

let rps = new RPS({ name: "Rock Paper Scissors" });
let player = new Player({ name: "RIKY" });
let com = new Computer({ name: "COM" });

window.setPlayerName = function () {
    if (rps.isNewGame()) {
        let name = prompt("Please enter your name:", player.getName());
        if (name) {
            player.setName(name.toUpperCase());
            document.getElementById("player-name").innerHTML = player.getName();
        }
    }
}

document.getElementById("game-name").innerHTML += rps.getName();
document.getElementById("player-name").innerHTML = player.getName();
document.getElementById("com-name").innerHTML = com.getName();

function getResultStyleClass(result) {
    let styleClass = "col-result";

    switch (result.toLowerCase()) {
        case "vs":
            styleClass += "-reset";
            break;

        case "draw":
            styleClass += "-draw";
            break;
    }

    return styleClass;
}

function setResultStyle(id, resultBefore, resultAfter) {
    let resultElement = document.getElementById(id);
    resultElement.innerHTML = resultAfter;

    resultElement.classList.replace(getResultStyleClass(resultBefore), getResultStyleClass(resultAfter));
}

function setChoiceBackgroundColor(id, backgroundColor) {
    if (backgroundColor) {
        document.getElementById("player-" + id[0]).style.backgroundColor = backgroundColor;
        document.getElementById("com-" + id[1]).style.backgroundColor = backgroundColor;
    } else {
        document.getElementById("player-" + id[0]).style.removeProperty("background-color");
        document.getElementById("com-" + id[1]).style.removeProperty("background-color");
    }
}

window.playerChoose = function (choice) {
    if (rps.isNewGame()) {
        player.choose(choice);
        console.log(player.getName() + " choose " + player.getChoiceName());

        rps.setIsNewGame(false);

        com.choose();
        console.log(com.getName() + " choose " + com.getChoiceName());

        setChoiceBackgroundColor([player.getChoice(), com.getChoice()], "#C4C4C4");

        let result = rps.getResult(player, com);
        setResultStyle("rps-result", "vs", result);
        console.log(result);
    }
}

window.restartRPS = function () {
    if (!rps.isNewGame()) {
        setChoiceBackgroundColor([player.getChoice(), com.getChoice()]);
        setResultStyle("rps-result", document.getElementById("rps-result").innerHTML, "vs");

        rps.setIsNewGame(true);
    }
}