const express = require('express');
const router = express.Router();
const auth = require('../controllers/authController');

/* GET login page. */
router.get('/login', auth.index);

router.post('/login', auth.login);

router.get('/logout', auth.logout);

module.exports = router;
