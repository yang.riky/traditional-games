'use strict';

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGameHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.UserGameHistory.belongsTo(models.UserGame, {
        foreignKey: 'userGameId'
      });
      models.UserGameHistory.belongsTo(models.GameRoom, {
        foreignKey: 'gameRoomId'
      });
    }
  }
  UserGameHistory.init({
    userChoice: {
      type: DataTypes.ENUM('ROCK', 'PAPER', 'SCISSORS'),
      allowNull: false
    },
    result: {
      type: DataTypes.ENUM('WIN', 'LOSE', 'DRAW')
    },
    score: {
      type: DataTypes.INTEGER
    },
    gameRound: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
  }, {
    sequelize,
    modelName: 'UserGameHistory',
    tableName: 'user_game_histories',
    underscored: true
  });
  return UserGameHistory;
};