function deleteUser(id, username) {
    if (confirm("Are you sure you want to delete user '" + username + "'?")) {
        location.replace("/dashboard/users/delete/" + id);
    }
}