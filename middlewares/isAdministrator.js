async function isAdministrator(req, res, next) {
    if (await req.session.userId) {
        next();
    } else {
        res.redirect('/auth/login');
    }
}

module.exports = isAdministrator;