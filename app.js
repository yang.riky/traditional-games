const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const sessions = require('express-session');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const playRouter = require('./routes/play');
const authRouter = require('./routes/auth');
const dashboardRouter = require('./routes/dashboard');
const usersRouter = require('./routes/users');
const gamesRouter = require('./routes/games');
const biodataRouter = require('./routes/biodata');
const registerRouter = require('./routes/register');
const loginRouter = require('./routes/login');
const roomsRouter = require('./routes/rooms');

const oneDay = 1000 * 60 * 60 * 24;

const app = express();

const URL_API = '/api/v1/';

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sessions({
  secret: "thisismysecrctekeyfhrgfgrfrty84fwir767",
  saveUninitialized: true,
  cookie: { maxAge: oneDay },
  resave: false
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/play', playRouter);
app.use('/auth', authRouter);
app.use('/dashboard', dashboardRouter);
app.use('/dashboard/users', usersRouter);
app.use('/dashboard/games', gamesRouter);
app.use('/dashboard/biodata', biodataRouter);
app.use(URL_API + 'register', registerRouter);
app.use(URL_API + 'login', loginRouter);
app.use(URL_API + 'rooms', roomsRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
