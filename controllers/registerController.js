const usersRepository = require('../repositories/usersRepository');

class RegisterController {

    async createUser(req, res, next) {
        try {
            const [err, countUser] = await usersRepository.countUserByUsername(req.body.username)
            if (countUser) {
                res.status(400).json({
                    msg: "Username already exists."
                });
            } else {
                const [err, userGame] = await usersRepository.createUser(
                    req.body.username,
                    req.body.email,
                    req.body.password
                );

                if (userGame) {
                    res.status(200).json({
                        msg: "Register berhasil, silahkan login",
                        userGame
                    });
                } else {
                    res.status(400).json({
                        err
                    });
                }
            }
        }
        catch (err) {
            res.status(400).json({
                msg: "Periksa kembali data data login anda",
                err
            });
        }
    }
}

const registerController = new RegisterController();

module.exports = registerController;
