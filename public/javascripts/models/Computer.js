import { Player } from "./Player.js";

export class Computer extends Player {

    choose() {
        super.choose(Math.floor(Math.random() * 3));
    }
}