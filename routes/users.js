const express = require('express');
const router = express.Router();
const isAdministrator = require('../middlewares/isAdministrator');
const usersController = require('../controllers/usersController');

router.get('/create', isAdministrator, usersController.add);

router.post('/create', isAdministrator, usersController.createUser);

/* GET users page. */
router.get('/', isAdministrator, usersController.getAllUser);

router.get('/:id', isAdministrator, usersController.getUserWithBiodataAndHistoryById);

router.get('/update/:id', isAdministrator, usersController.edit);

router.post('/update/:id', isAdministrator, usersController.updateUserById);

router.get('/delete/:id', isAdministrator, usersController.deleteUserById);

module.exports = router;
