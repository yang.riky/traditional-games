const usersRepository = require('../repositories/usersRepository');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

class LoginController {

    async loginUser(req, res, next) {
        try {
            const [err, userGame] = await usersRepository.getUserByEmail(req.body.email);
            let login = false;

            if (userGame) {
                const match = await bcrypt.compare(req.body.password, userGame.password);

                if (match) {
                    login = true;
                    jwt.sign({
                        email: userGame.email,
                        id: userGame.id
                    }, 'secret_key', function (err, token) {
                        res.status(200).json({
                            msg: "Successfully login",
                            token
                        });
                    });
                }
            }

            if (!login) {
                res.status(400).json({
                    msg: 'Invalid email or password'
                });
            }
        }
        catch (err) {
            res.status(400).json({
                err
            });
        }
    }
}

const loginController = new LoginController();

module.exports = loginController;
