'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GameRoom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.GameRoom.belongsTo(models.UserGame, {
        foreignKey: 'user_game_id'
      });
      models.GameRoom.belongsTo(models.UserGame, {
        foreignKey: 'opponent_game_id'
      });
      models.GameRoom.hasMany(models.UserGameHistory, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    }
  }
  GameRoom.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
        len: [3, 255]
      }
    },
    code: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
        len: [5]
      }
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
      validate: {
        notNull: true
      }
    }
  }, {
    sequelize,
    modelName: 'GameRoom',
    tableName: 'game_rooms',
    underscored: true,
    paranoid: true
  });
  return GameRoom;
};