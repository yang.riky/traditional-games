export class Player {

  name = "";
  choices = ["ROCK", "PAPER", "SCISSORS"];
  choice = 0;

  constructor(player) {
    this.name = player.name;
  }

  getName() {
    return this.name;
  }

  setName(name) {
    this.name = name;
  }

  choose(choice) {
    this.choice = choice;
  }

  getChoice() {
    return this.choice;
  }

  getChoiceName() {
    return this.choices[this.choice];
  }
}