import { Game } from "./Game.js";

export class RPS extends Game {

    getResult(player, computer) {
        let result = " WIN";
        let diff = player.getChoice() - computer.getChoice();

        switch (diff) {
            case -1:
            case 2:
                result = computer.getName() + result;
                break;

            case 1:
            case -2:
                result = player.getName() + result;
                break;

            default:
                result = "DRAW";
                break;
        }

        return result;
    }

}