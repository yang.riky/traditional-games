const express = require('express');
const router = express.Router();
const roomsController = require('../controllers/roomsController');
const checkJwtAuthentication = require('../middlewares/checkJwtAuthentication')

router.post('/create', checkJwtAuthentication, roomsController.createRoom);
router.post('/join/:roomCode', checkJwtAuthentication, roomsController.joinRoom);
router.post('/fight/:roomCode', checkJwtAuthentication, roomsController.fightRoom);
router.get('/', checkJwtAuthentication, roomsController.getAllRooms)

module.exports = router;
