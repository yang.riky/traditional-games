const express = require('express');
const dashboard = require('../controllers/dashboardController');
const router = express.Router();
const isAdministrator = require('../middlewares/isAdministrator');

/* GET admin page. */
router.get('/', isAdministrator, dashboard);

module.exports = router;
