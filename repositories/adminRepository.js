const { UserAdmin } = require('../models');

class AdminRepository {

    async getAdminByEmail(email) {
        try {
            const res =  await UserAdmin.findOne({
                where: {
                    email
                },
                attributes: ['email', 'password']
            });

            return [null, res];
        }
        catch (err) {
          return [err, null];
        }
    }
}

const adminRepository = new AdminRepository();

module.exports = adminRepository;
