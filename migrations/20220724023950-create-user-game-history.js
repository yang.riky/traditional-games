'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('user_game_histories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userGameId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        field: 'user_game_id',
        references: {
          model: 'user_games',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      },
      userChoice: {
        type: Sequelize.ENUM('ROCK', 'PAPER', 'SCISSORS'),
        allowNull: false,
        field: 'user_choice'
      },
      computerChoice: {
        type: Sequelize.ENUM('ROCK', 'PAPER', 'SCISSORS'),
        allowNull: false,
        field: 'computer_choice'
      },
      result: {
        type: Sequelize.ENUM('WIN', 'LOSE', 'DRAW')
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at'
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'updated_at'
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('user_game_histories');
  }
};