export class Game {

    name = "";
    newGame = true;

    constructor(game) {
        if (this.constructor === Game) {
            throw new Error("Cannot instantiate from Abstract Class");
        }

        this.name = game.name;
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }

    isNewGame() {
        return this.newGame;
    }

    setIsNewGame(isNewGame) {
        this.newGame = isNewGame;
    }
}