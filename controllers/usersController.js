const createError = require('http-errors');
const usersRepository = require('../repositories/usersRepository');

class UsersController {
    async add(req, res, next) {
        await res.render('users/create', {
            title: 'Users', sectionTitle: 'Create User', action: 'create',
            button: '',
            "value": {
                "username": "", "email": ""
            },
            alert: null
        });
    }

    async createUser(req, res, next) {
        try {
          const [err, userCount] = await usersRepository.countUserByUsername(req.body.username);
            if (userCount) {
                res.render('users/create', {
                    title: 'Users', sectionTitle: 'Create User', action: 'create',
                    button: '',
                    "value": {
                        "username": req.body.username, "email": req.body.email
                    },
                    alert:
                        `<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                </symbol>
                </svg>
                <div class="mt-1 alert alert-danger d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                    <div>
                    Username already exists.
                    </div>
                </div>`
                });
            } else {
                const [err, userGame] = await usersRepository.createUser(
                    req.body.username,
                    req.body.email,
                    req.body.password
                );

if (userGame) {
  res.redirect('/dashboard/users/');
} else {
  next(createError(400));
}
            }
        }
        catch (err) {
            next(createError(400));
        }
    }

    async getAllUser(req, res, next) {
        try {
            const [err, userGames] = await usersRepository.getAllUser();

            res.render('users/index', {
                title: 'Users',
                sectionTitle: 'List of Users',
                button: `<a class= "btn btn-success" href="/dashboard/users/create" role="button">
                <span data-feather="plus"></span></a>`,
                userGames
            });
        }
        catch (err) {
            next(createError(400));
        }
    }

    async getUserWithBiodataAndHistoryById(req, res, next) {
        try {
            const [err, userGame] = await usersRepository.getUserWithBiodataAndHistoryById(req.params.id);

            res.render('biodata/create', {
                title: 'Users',
                sectionTitle: userGame.username + " (" + userGame.email + ")",
                button: '',
                userGame
            });
        }
        catch (err) {
            next(createError(400));
        }
    }

    async edit(req, res, next) {
        try {
            const [err, userGame] = await usersRepository.getUserWithBiodataAndHistoryById(req.params.id);

            res.render('users/create', {
                title: 'Users', sectionTitle: 'Update User', action: 'update',
                button: '',
                "value": userGame,
                alert: null
            });
        }
        catch (err) {
            next(createError(400));
        }
    }

    async updateUserById(req, res, next) {
        try {
            const [err, isUserIdExist] = await usersRepository.countUserById(req.params.id);

            if (isUserIdExist) {
                const [err, userGame] = await usersRepository.getUserByUsernameForUpdate(
                    req.body.username, req.params.id);

                if (userGame) {
                    const [err, updateUserById] = await usersRepository.updateUserById(
                        req.body.username, req.body.email, req.body.password, req.params.id);

                    res.redirect('/dashboard/users/');
                } else {
                    res.render('users/create', {
                        title: 'Users', sectionTitle: 'Update User', action: 'update',
                        button: '',
                        "value": {
                            "username": req.body.username, "email": req.body.email
                        },
                        alert:
                            `<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                </symbol>
                </svg>
                <div class="mt-1 alert alert-danger d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                    <div>
                    Username already exists.
                    </div>
                </div>`
                    });
                }
            } else {
                res.render('users/create', {
                    title: 'Users', sectionTitle: 'Update User', action: 'update',
                    button: '',
                    "value": {
                        "username": req.body.username, "email": req.body.email
                    },
                    alert:
                        `<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                </symbol>
                </svg>
                <div class="mt-1 alert alert-danger d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                    <div>
                    User not found.
                    </div>
                </div>`
                });
            }
        }
        catch (err) {
            next(createError(400));
        }
    }

    async deleteUserById(req, res, next) {
        try {
            const [err, deleteUserById] = await usersRepository.deleteUserById(req.params.id);

            res.redirect('/dashboard/users/');
        }
        catch (err) {
            next(createError(400));
        }
    }
}

const usersController = new UsersController();

module.exports = usersController;
