const express = require('express');
const router = express.Router();
const isAdministrator = require('../middlewares/isAdministrator');
const biodataController = require('../controllers/biodataController');

router.post('/create/:userId', isAdministrator, biodataController.createBiodata);

module.exports = router;
