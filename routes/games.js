const express = require('express');
const router = express.Router();
const isAdministrator = require('../middlewares/isAdministrator');
const gamesController = require('../controllers/gamesController');

/* GET games page. */
router.get('/', isAdministrator, gamesController.getAllGames);

module.exports = router;
