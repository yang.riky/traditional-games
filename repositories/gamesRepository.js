const { UserGameHistory, UserGame } = require('../models');
const { Op } = require('sequelize');

class GamesRepository {

    async getAllGames() {
        try {
            const res = await UserGameHistory.findAll({
                attributes: ['id', 'userChoice', 'computerChoice', 'result', 'createdAt', 'updatedAt'],
                include: {
                    model: UserGame,
                    attributes: ['username']
                }
            })

            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async getLastGameRound(gameRoomId, user_game_id) {
        try {
            const res = await UserGameHistory.findOne({
                where: {
                    gameRoomId,
                    user_game_id
                },
                order: [['id', 'DESC']]
            });
            return [null, res];
        } catch (err) {
            return [err, null];
        }
    }

    async countGameRound(gameRoomId, gameRound) {
        try {
            const res = await UserGameHistory.count({
                where: {
                    gameRoomId,
                    gameRound
                }
            });
            return [null, res];
        } catch (err) {
            return [err, null];
        }
    }

    async countGameRoundByUserGameId(gameRoomId, gameRound, userGameId) {
        try {
            const res = await UserGameHistory.count({
                where: {
                    gameRoomId,
                    gameRound,
                    userGameId
                }
            });
            return [null, res];
        } catch (err) {
            return [err, null];
        }
    }

    async addGame(userGameId, userChoice, gameRoomId, gameRound) {
        try {
            const res = await UserGameHistory.create({
                userGameId,
                userChoice,
                gameRoomId,
                gameRound
            });
            return [null, res];
        } catch (err) {
            return [err, null];
        }
    }

    async getOpponentGame(gameRoomId, gameRound, opponentGameId) {
        try {
            const res = await UserGameHistory.findOne({
                where: {
                    gameRoomId,
                    gameRound,
                    userGameId: {
                        [Op.not]: opponentGameId
                    }
                }
            });
            return [null, res];
        } catch (err) {
            return [err, null];
        }
    }

    async updateGameResultAndScore(id, result, score) {
        try {
            const res = await UserGameHistory.update({
                result,
                score
            }, {
                where: {
                    id
                }
            });
            return [null, res];
        } catch (err) {
            return [err, null];
        }
    }
}

const gamesRepository = new GamesRepository();

module.exports = gamesRepository;
