'use strict';
const bcrypt = require('bcrypt');

async function hashPassword(password) {
  const SALT_ROUNDS = 10;

  return await bcrypt.hash(password, SALT_ROUNDS);
}

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.UserGame.hasOne(models.UserGameBiodata, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      });
      models.UserGame.hasMany(models.UserGameHistory, {
        onDelete: 'RESTRICT',
        onUpdate: 'CASCADE'
      });
      models.UserGame.hasMany(models.GameRoom, {
        foreignKey: "user_game_id"
      }, {
        onDelete: 'RESTRICT',
        onUpdate: 'CASCADE'
      })
      models.UserGame.hasMany(models.GameRoom, {
        foreignKey: "opponent_game_id"
      }, {
        onDelete: 'RESTRICT',
        onUpdate: 'CASCADE'
      })
    }
  }
  UserGame.init({
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notNull: true,
        notEmpty: true,
        len: [3, 255]
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notNull: true,
        notEmpty: true,
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
        len: [6, 20]
      }
    }
  }, {
    hooks: {
      beforeCreate: async (userGame, options) => {
        userGame.password = await hashPassword(userGame.password);
      },
      beforeUpdate: async (userGame, options) => {
        userGame.password = await hashPassword(userGame.password);
      }
    },
    sequelize,
    modelName: 'UserGame',
    tableName: 'user_games',
    underscored: true,
    paranoid: true
  });
  return UserGame;
};