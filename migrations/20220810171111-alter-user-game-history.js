'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.removeColumn('user_game_histories', 'computer_choice');

    await queryInterface.addColumn('user_game_histories', 'score', {
      type: Sequelize.INTEGER
    });

    await queryInterface.addColumn('user_game_histories', 'game_room_id', {
      type: Sequelize.INTEGER,
      // allowNull: false,
      references: {
        model: 'game_rooms',
        key: 'id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'RESTRICT'
    });

    await queryInterface.addColumn('user_game_histories', 'game_round', {
      type: Sequelize.INTEGER,
      // allowNull: false,
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.addColumn('user_game_histories', 'computer_choice', {
      type: Sequelize.ENUM('ROCK', 'PAPER', 'SCISSORS'),
      allowNull: false,
      field: 'computer_choice'
    });

    await queryInterface.removeColumn('user_game_histories', 'score');

    await queryInterface.removeColumn('user_game_histories', 'room_id');

    await queryInterface.removeColumn('user_game_histories', 'game_round');
  }
};
