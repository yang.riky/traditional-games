const { UserGame, UserGameBiodata, UserGameHistory, sequelize } = require('../models');
const { Op } = require('sequelize');

class UserRepository {

    async createUser(username, email, password) {
        try {
            const res = await UserGame.create({
                username,
                email,
                password
            });

            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async countUserById(id) {
        try {
            const res = await UserGame.count({
                where: { id }
            })
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async countUserByUsername(username) {
        try {
            const res = await UserGame.count({
                where: { username }
            })
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async getAllUser() {
        try {
            const res = await UserGame.findAll({
                attributes: ['id', 'username', 'email', 'createdAt', 'updatedAt']
            });
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async getUserWithBiodataAndHistoryById(id) {
        try {
            const res = await UserGame.findOne({
                where:
                    { id },
                attributes:
                    ['id', 'username', 'email'],
                include: [{
                    model: UserGameBiodata,
                    attributes: ['firstName', 'lastName']
                },
                {
                    model: UserGameHistory,
                    attributes: ['id', 'userChoice', 'computerChoice', 'result', 'createdAt', 'updatedAt']
                }]
            })
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async getUserById(id) {
        try {
            const res = await UserGame.findOne({
                where: {
                    id
                },
                attributes: ['id', 'username', 'email']
            })
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async getUserByEmail(email) {
        try {
            const res = await UserGame.findOne({
                where: {
                    email
                },
                attributes: ['id', 'email', 'password']
            })
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async getUserByUsernameForUpdate(username, id) {
        try {
            const res = await UserGame.findOne({
                where: {
                    username,
                    [Op.not]: [{ id }]
                },
                attributes: ['id', 'username', 'email']
            });
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async updateUserById(username, email, password, id) {
        try {
            const res = await UserGame.update({
                username,
                email,
                password
            },
                {
                    where: {
                        id
                    }
                })
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async deleteUserById(id) {
        try {
            const res = await UserGame.destroy({
                where: {
                    id
                }
            })
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }
}

const usersRepository = new UserRepository();

module.exports = usersRepository;
