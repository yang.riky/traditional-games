const createError = require('http-errors');
const biodataRepository = require('../repositories/biodataRepository');

class BiodataController {

    async createBiodata(req, res, next) {
        try {
            const [err, biodata] = await biodataRepository.findOrCreateBiodataByUserGameId(
                req.body.firstName,
                req.body.lastName,
                req.params.userId
            );

            if (biodata) {
              const [err, biodata] = await biodataRepository.updateBiodataByUserGameId(
                  req.body.firstName,
                  req.body.lastName,
                  req.params.userId
              );

              if (biodata) {
                res.redirect('/dashboard/users/' + req.params.userId);
              } else {
                next(createError(400));
              }
            } else {
              next(createError(400));
            }
        }
        catch (err) {
            next(createError(400));
        }
    }
}

const biodataController = new BiodataController();

module.exports = biodataController;
