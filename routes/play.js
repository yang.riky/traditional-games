const express = require('express');
const play = require('../controllers/playController');
const router = express.Router();

/* GET play page. */
router.get('/', play);

module.exports = router;
