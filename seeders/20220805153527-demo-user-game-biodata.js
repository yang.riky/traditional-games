'use strict';

const { UserGame } = require('../models');
const { faker } = require('@faker-js/faker');

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const userGames = await UserGame.findAll({
      attributes: ['id']
    });

    const newData = [];

    for (let i = 0; i < 20; i++) {
      const seedData = {
        user_game_id: userGames[Math.floor(Math.random() * userGames.length)].id,
        first_name: faker.name.firstName(),
        last_name: faker.name.lastName(),
        created_at: new Date(),
        updated_at: new Date()
      };

      newData.push(seedData);
    }

    await queryInterface.bulkInsert('user_game_biodata', newData, {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    await queryInterface.bulkDelete('user_game_biodata', null, {});
  }
};
