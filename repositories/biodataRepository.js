const { UserGameBiodata } = require('../models');

class BiodataRepository {

    async updateBiodataByUserGameId(firstName, lastName, UserGameId) {
        try {
            const res = await UserGameBiodata.update({
                firstName,
                lastName
            },
                {
                    where: {
                        UserGameId
                    }
                })

                return [null, res];
        }
        catch (err) {
          return [err, null];
        }
    }

    async findOrCreateBiodataByUserGameId(firstName, lastName, UserGameId) {
        try {
            const res = await UserGameBiodata.findOrCreate({
                where: {
                    UserGameId
                },
                defaults: {
                    firstName,
                    lastName
                }
            })

            return [null, res];
        }
        catch (err) {
          return [err, null];
        }
    }
}

const biodataRepository = new BiodataRepository();

module.exports = biodataRepository;
