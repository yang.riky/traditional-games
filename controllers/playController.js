async function play(req, res, next) {
    await res.render('play', { choices: ["batu", "kertas", "gunting"] });
}

module.exports = play;