'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const newData = [];

    const seedData = {
      username: "riky",
      email: "riky@binar.com",
      password: "rikyGames",
      created_at: new Date(),
      updated_at: new Date()
    };

    newData.push(seedData);

    await queryInterface.bulkInsert('user_admins', newData, {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('user_admins', null, {});
  }
};
