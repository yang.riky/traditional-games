const { GameRoom, UserGame, UserGameHistory, sequelize } = require('../models');
const { Op } = require('sequelize');

class RoomsRepository {

    async getAllRoomsWithHistory(user_game_id) {
        try {
            const res = await GameRoom.findAll({
                where: {
                    [Op.or]: [
                        { user_game_id: user_game_id },
                        { opponent_game_id: user_game_id }
                    ]
                },
                include: [{
                    model: UserGame,
                    attributes: ['username']
                }, {
                    model: UserGameHistory
                }]
            });
            return [null, res];
        } catch (err) {
            return [err, null];
        }
    }

    async getRoomWithHistory(id) {
        try {
            const res = await GameRoom.findOne({
                where: {
                    id
                },
                include: [{
                    model: UserGame,
                    attributes: ['username']
                }, {
                    model: UserGameHistory
                }]
            });
            return [null, res];
        } catch (err) {
            return [err, null];
        }
    }

    async countRoomCode(roomCode) {
        try {
            const res = await GameRoom.count({
                where: {
                    code: roomCode,
                    status: true
                }
            })
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async getRoomByRoomCode(roomCode) {
        try {
            const res = await GameRoom.findOne({
                where: {
                    code: roomCode,
                    status: true
                }
            })
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async getRoomForFight(roomCode, user_game_id) {
        try {
            const res = await GameRoom.findOne({
                where: {
                    code: roomCode,
                    status: false,
                    [Op.or]: [
                        { user_game_id: user_game_id },
                        { opponent_game_id: user_game_id }
                    ]
                }
            });
            return [null, res];
        } catch (err) {
            return [err, null];
        }
    }

    async countRoomCodeByUserId(roomCode, user_game_id) {
        try {
            const res = await GameRoom.count({
                where: {
                    code: roomCode,
                    status: true,
                    user_game_id
                }
            })
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }

    async updateRoomByOpponentId(id, opponent_game_id) {
        try {
            const res = await GameRoom.update({
                opponent_game_id,
                status: false
            }, {
                where: {
                    id
                }
            });
            return [null, res];
        }
        catch (err) {
            return [null, res];
        }
    }

    async findOrCreateRoomByUserGameId(roomName, roomCode, user_game_id) {
        try {
            const res = await GameRoom.findOrCreate({
                where: {
                    user_game_id,
                    status: true
                },
                defaults: {
                    name: roomName,
                    code: roomCode,
                    user_game_id
                }
            })
            return [null, res];
        }
        catch (err) {
            return [err, null];
        }
    }
}

const roomsRepository = new RoomsRepository();

module.exports = roomsRepository;
