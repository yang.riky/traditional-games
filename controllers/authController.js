const createError = require('http-errors');
const bcrypt = require('bcrypt');
const adminRepository = require('../repositories/adminRepository');

function index(req, res, next) {
    if (req.session.userId) {
        res.redirect('/dashboard');
    } else {
        res.render('login', { alert: null });
    }
}

async function login(req, res, next) {
    try {
        const [err, userAdmin] = await adminRepository.getAdminByEmail(req.body.email);
        let login = false;

        if (userAdmin) {
            const match = await bcrypt.compare(req.body.password, userAdmin.password);

            if (match) {
                login = true;
                req.session.userId = req.body.email;
                res.redirect('/dashboard');
            }
        } else {
          next(createError(400));
        }

        if (!login) {
            res.render('login', {
                alert:
                    `<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                </symbol>
                </svg>
                <div class="mt-1 alert alert-danger d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                    <div>
                    Invalid email or password
                    </div>
                </div>`
            });
        }
    }
    catch (err) {
        next(createError(500));
    }
}

function logout(req, res, next) {
    req.session.destroy();
    res.redirect('/dashboard');
}

module.exports = { index, login, logout };
